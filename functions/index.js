/**
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const {logger} = require("firebase-functions");
const sanitizer = require("./sanitizer");
const { Translate } = require('@google-cloud/translate').v2;
const {Firestore, FieldValue} = require('@google-cloud/firestore');
const nodemailer = require('nodemailer');

const firestore = new Firestore();
const translate = new Translate();

function DBN(name, dev) {
  return name + (dev? '-dev': '');
}

async function translateData(data, translateProperties, htmlTranslateProperties) {
  if(!htmlTranslateProperties) htmlTranslateProperties = [];
  const translatedData = {};
  var promises = [];
  for(let key of Object.keys(data)) {
    let tempData = [];
    if(translateProperties.includes(key)) {
      promises.push(translate.translate(data[key], 'si')
        .then(res => {tempData[0] = res[0]}));
      promises.push(translate.translate(data[key], 'en')
        .then(res => {tempData[1] = res[0]}));
      promises.push(translate.translate(data[key], 'ta')
        .then(res => {tempData[2] = res[0]}));
      tempData[3] = data[key];
      translatedData[key] = tempData;
    } else if(htmlTranslateProperties.includes(key)) {
      promises.push(translate.translate(data[key], {to: 'si', format: 'html'})
      .then(res => {tempData[0] = res[0]}));
      promises.push(translate.translate(data[key], {to: 'en', format: 'html'})
      .then(res => {tempData[1] = res[0]}));
      promises.push(translate.translate(data[key], {to: 'ta', format: 'html'})
      .then(res => {tempData[2] = res[0]}));
      tempData[3] = data[key];
      translatedData[key] = tempData;
    } else {
      translatedData[key] = data[key];
    }
  }
  await Promise.all(promises);

  // add machine translated flag
  Object.keys(translatedData).map(key => {
    if(translateProperties.includes(key) || htmlTranslateProperties.includes(key)) {
      translatedData[key + '_MT'] = [
        (translatedData[key][0] != translatedData[key][3]),
        (translatedData[key][1] != translatedData[key][3]),
        (translatedData[key][2] != translatedData[key][3])
      ]
    }
  });

  return translatedData;
}

exports.addelection = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  const translateProperties = [
    'title',
    'description'
  ];
  const translatedData = await translateData(data, translateProperties);

  logger.info('Translated data :', translatedData);
  // Enter new data into the document.
  let document = firestore.collection(DBN('Elections', dev)).doc(data.url);
  translatedData.id = document.id;
  await document.set(translatedData);

  return translatedData;
});

exports.addproposal = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  const translateProperties = [
    'name'
  ];
  const translateHTMLProperties = [
    'description',
    'purpose'
  ];
  const translatedData = await translateData(data, translateProperties, translateHTMLProperties);

  logger.info('Translated data :', translatedData);
  // Enter new data into the document.
  let document = firestore.collection(DBN('Proposals', dev)).doc();
  translatedData.id = document.id;
  await document.set(translatedData);

  return translatedData;
});

exports.addnomination = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  const translateProperties = [
    'title',
    'description'
  ];
  const translatedData = await translateData(data, translateProperties);

  logger.info('Translated data :', translatedData);
  // Enter new data into the document.
  let document = firestore.collection(DBN('Nominations', dev)).doc();
  translatedData.id = document.id;
  await document.set(translatedData);

  return translatedData;
});

exports.addcomment = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;
  if(dev) {
    return addcommentDev(data, context);
  }

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  const translateProperties = ['content'];
  const translatedData = await translateData(data, translateProperties);

  logger.info('Translated data :', translatedData);
  // Enter new data into the document.
  let document = firestore.collection(DBN('Comments', dev)).doc(translatedData.id);

  const parent = {
    collection: translatedData.collection,
    id: translatedData.id
  };
  delete translatedData.collection;
  delete translatedData.id;

  await document.set({
    comments: FieldValue.arrayUnion(translatedData),
    parent
  }, { merge: true });

  return translatedData;
});

function addcommentDev(data, context) {
  logger.info('Running dev function');
  logger.info('Input data (dev) :', data);
  const dev = !!data.dev;

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  const translateProperties = ['content'];
  const translatedData = await translateData(data, translateProperties);

  const parentId = translateData.id;
  delete translateData.id;

  logger.info('Translated data :', translatedData);
  // Enter new data into the document.
  let document = firestore.collection(DBN('Comments', dev)).doc();

  await document.set({
    ...translateData,
    id: document.id
    parentId
  });

  return translatedData;
}

exports.addbulletin = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  const translateProperties = ['title'];
  const htmlTranslateProperties = ['description'];
  const translatedData = await translateData(data, translateProperties, htmlTranslateProperties);

  logger.info('Translated data :', translatedData);
  // Enter new data into the document.
  let document = firestore.collection(DBN('Bulletin', dev)).doc();
  translatedData.id = document.id;
  await document.set(translatedData);

  return translatedData;
});

exports.addvote = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  const nominationId = data.nominationId;
  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  let getPromises = [];
  getPromises.push(firestore.collection(DBN('Users', dev)).doc(context.auth.uid).get());
  getPromises.push(firestore.collection(DBN('Nominations', dev)).doc(nominationId).get());
  let results = await Promise.all(getPromises);
  let user = results[0].data();
  let nomination = results[1].data();
  let election = (await firestore.collection(DBN('Elections', dev)).doc(nomination.election).get()).data();

  logger.info(user, nomination, election);

  let userVoteCount = 0;
  user.votedFor.map((vote) => {
    if(vote.election == nomination.election) userVoteCount++;
  });
  logger.info('vote count', userVoteCount);

  let userVoted = !!user.votedFor.find((vote) => {
    return (vote.nomination == nomination);
  });
  logger.info('user voted', userVoted);

  let nominationVoted = !!nomination.votedBy.find((vote) => {
    if(vote == context.auth.uid) return true;
  });
  logger.info('nomination voted', nominationVoted);

  if((election.maxVotes <= userVoteCount) && (!nominationVoted && !userVoted)) {
    logger.info('no votes left');
    return { success: false, message: 'no votes left', code: 1}
  }

  if(!nominationVoted && !userVoted) {
    try {
      let setPromises = [];
      setPromises.push(firestore.collection(DBN('Users', dev)).doc(context.auth.uid).update({
        votedFor: FieldValue.arrayUnion({nomination: nominationId, election: election.url})
      }));
      setPromises.push(firestore.collection(DBN('Nominations', dev)).doc(nominationId).update({
        votedBy: FieldValue.arrayUnion(context.auth.uid),
        votes: FieldValue.increment(1)
      }));
      setPromises.push(firestore.collection(DBN('Elections', dev)).doc(nomination.election).update({
        votes: FieldValue.increment(1)
      }));
      await Promise.all(setPromises);
      return { success: true, message: 'vote added', code: -1}
    } catch (e) {
      logger.info(e);
      return { success: false, message: 'update failed', code: 2}
    }
  } else {
    try {
      let setPromises = [];
      setPromises.push(firestore.collection(DBN('Users', dev)).doc(context.auth.uid).update({
        votedFor: FieldValue.arrayRemove({nomination: nominationId, election: election.url})
      }));
      setPromises.push(firestore.collection(DBN('Nominations', dev)).doc(nominationId).update({
        votedBy: FieldValue.arrayRemove(context.auth.uid),
        votes: FieldValue.increment(-1)
      }));
      setPromises.push(firestore.collection(DBN('Elections', dev)).doc(nomination.election).update({
        votes: FieldValue.increment(-1)
      }));
      await Promise.all(setPromises);
      return { success: true, message: 'vote removed', code: -2}
    } catch (e) {
      logger.info(e);
      return { success: false, message: 'update failed', code: 3}
    }
  }
});

exports.deletenomination = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  const nominationId = data.nominationId;

  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  let nomination = (await firestore.collection(DBN('Nominations', dev)).doc(nominationId).get()).data();

  if(nomination.createdBy == context.auth.uid) {
    await firestore.collection(DBN('Nominations', dev)).doc(nominationId).delete();
    return { success: true, message: 'item deleted', code: -1}
  } else {
    return { success: false, message: 'no permissions', code: 1}
  }
});

exports.updatetext = functions.https.onCall(async (data, context) => {
  logger.info('Input data :', data);
  const dev = !!data.dev;

  logger.info('IAuth :', context.auth);
  if (!context.auth) {
    throw new HttpsError("failed-precondition", "The function must be " +
            "called while authenticated.");
  }

  let doc = (await firestore.collection(DBN(data.collection, dev)).doc(data.id).get()).data();

  if((doc.createdBy != context.auth.uid) && !context.auth.token.admin) {
    return { success: false, message: 'no permissions' }
  }

  if(doc.createdBy == context.auth.uid) {
    await firestore.collection(DBN(data.collection, dev)).doc(data.id).update({
      [data.path]: data.data,
      [data.path + '_MT']: data.mt,
      edits: FieldValue.arrayUnion({
        old: doc[data.path],
        old_MT: doc[data.path + '_MT'],
        new: data.data,
        new_MT: data.mt,
        createdBy: context.auth.uid,
        createdOn: (new Date()).getTime()
      })
    });
    return { success: true, message: 'item updated'}
  } else {
    if(context.auth.token.admin) {
      const edit = firestore.collection(DBN('Edits', dev)).doc();
      await edit.set({
        ...data,
        editId: edit.id,
        suggestedBy: context.auth.uid
      });

      let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        auth: {
          user: "contact@aragalaya.online",
          pass: "thpqrzzezjnmlemm"
        }
      });

      const approveHTML = approveHTMLTemplate
        .replaceAll('hash', edit.id + (dev? '&dev=true': ''))
        .replaceAll('{{POST}}', doc[data.path][data.language])
        .replaceAll('{{EDIT}}', data.data[data.language]);

      let user = (await firestore.collection(DBN('Users', dev)).doc(doc.createdBy).get()).data();

      const mailOptions = {
        from: 'contact@aragalaya.online', //Adding sender's email
        to: user.email, //Getting recipient's email by query string
        subject: 'Please review and approve this edit suggested by an admin', //Email subject
        html: approveHTML //Email content in HTML
      };

      const mailResponse = await new Promise(resolve => {
        return transporter.sendMail(mailOptions, (err, info) => {
          if(err) {
            resolve(err);
          }
          return resolve(info);
        });
      });
      return mailResponse;
    }
  }
});

exports.approveedit = functions.https.onRequest(async (req, res) => {
  logger.info('approving :', req.query.approve, req.query.dev);
  const data = (await firestore.collection(DBN('Edits', req.query.dev)).doc(req.query.approve).get()).data();
  const doc = (await firestore.collection(DBN(data.collection, req.query.dev)).doc(data.id).get()).data();
  await firestore.collection(DBN(data.collection, req.query.dev)).doc(data.id).update({
    [data.path]: data.data,
    [data.path + '_MT']: data.mt,
    edits: FieldValue.arrayUnion({
      old: doc[data.path],
      old_MT: doc[data.path + '_MT'],
      new: data.data,
      new_MT: data.mt,
      createdBy: data.suggestedBy,
      createdOn: (new Date()).getTime()
    })
  });
  res.send(200);
});

const approveHTMLTemplate = `
<!DOCTYPE html>

<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title></title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->
<style>
		* {
			box-sizing: border-box;
		}

		body {
			margin: 0;
			padding: 0;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: inherit !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
		}

		p {
			line-height: inherit
		}

		.desktop_hide,
		.desktop_hide table {
			mso-hide: all;
			display: none;
			max-height: 0px;
			overflow: hidden;
		}

		@media (max-width:520px) {
			.desktop_hide table.icons-inner {
				display: inline-block !important;
			}

			.icons-inner {
				text-align: center;
			}

			.icons-inner td {
				margin: 0 auto;
			}

			.row-content {
				width: 100% !important;
			}

			.column .border,
			.mobile_hide {
				display: none;
			}

			table {
				table-layout: fixed !important;
			}

			.stack .column {
				width: 100%;
				display: block;
			}

			.mobile_hide {
				min-height: 0;
				max-height: 0;
				max-width: 0;
				overflow: hidden;
				font-size: 0px;
			}

			.desktop_hide,
			.desktop_hide table {
				display: table !important;
				max-height: none !important;
			}
		}
	</style>
</head>
<body style="background-color: #FFFFFF; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 500px;" width="500">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="color:#393d47;font-size:14px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;font-weight:400;line-height:120%;text-align:left;direction:ltr;letter-spacing:0px;">
<p style="margin: 0;">aragalaya.online හි ඔබ විසින් කරන ලද සටහනකට පරිපාලකයෙකු පහත වෙනස් කිරීම යෝජනා කර ඇත.<br/><br/>ඔබ මෙම වෙනසට එකඟ වන්නේ නම් <a href="https://us-central1-sldemands.cloudfunctions.net/approveedit?approve=hash" rel="noopener" style="text-decoration: underline; color: #8a3b8f;" target="_blank">එය අනුමත කිරීමට මෙහි ක්ලික් කරන්න.</a></p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="color:#393d47;font-size:14px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;font-weight:400;line-height:120%;text-align:left;direction:ltr;letter-spacing:0px;">
<p style="margin: 0; margin-bottom: 16px;">An admin has suggested the below edit for one of the posts you made in aragalaya.online.</p>
<p style="margin: 0;">If you agree with it, <a href="https://us-central1-sldemands.cloudfunctions.net/approveedit?approve=hash" rel="noopener" style="text-decoration: underline; color: #8a3b8f;" target="_blank">click here to approve it.</a></p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="color:#393d47;font-size:14px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;font-weight:400;line-height:120%;text-align:left;direction:ltr;letter-spacing:0px;">
<p style="margin: 0;">aragalaya.online இல் நீங்கள் செய்த இடுகைக்கு கீழே உள்ள மாற்றத்தை நிர்வாகி ஒருவர் பரிந்துரைத்துள்ளார்.<br/><br/>இந்த மாற்றத்தை நீங்கள் ஏற்றுக்கொண்டால் அதை <a href="https://us-central1-sldemands.cloudfunctions.net/approveedit?approve=hash" rel="noopener" style="text-decoration: underline; color: #8a3b8f;" target="_blank">அங்கீகரிக்க இங்கே கிளிக் செய்யவும்.</a></p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="color:#393d47;font-size:14px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;font-weight:400;line-height:120%;text-align:left;direction:ltr;letter-spacing:0px;">
<p style="margin: 0; margin-bottom: 16px;">Post:</p>
<p style="margin: 0; margin-bottom: 16px;">{{POST}}</p>
<p style="margin: 0; margin-bottom: 16px;">Edit:</p>
<p style="margin: 0;">{{EDIT}}</p>
</div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 500px;" width="500">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="icons_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td style="vertical-align: middle; color: #9d9d9d; font-family: inherit; font-size: 15px; padding-bottom: 5px; padding-top: 5px; text-align: center;">
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table><!-- End -->
</body>
</html>
`;
